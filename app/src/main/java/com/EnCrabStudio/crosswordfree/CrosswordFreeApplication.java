package com.EnCrabStudio.crosswordfree;

import android.app.Application;
import android.content.res.Configuration;
import android.location.Location;

import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.helper.TypefaceUtil;
import com.EnCrabStudio.crosswordfree.manager.InterstitialAdManager;
import com.EnCrabStudio.crosswordfree.manager.RatingManager;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;
import com.EnCrabStudio.crosswordfree.manager.TypefaceManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.Locale;

/**
 * Created by khanhnguyen on 29/09/2016
 */
public class CrosswordFreeApplication extends Application {

    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        String language = getString(R.string.language);

        try {
            Locale.setDefault(new Locale(language));
            Configuration config = new Configuration();
            config.locale = new Locale(language);
            getResources().updateConfiguration(config, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SoundManager.init(this);
        RatingManager.init(this);
        TypefaceManager.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }



    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

}

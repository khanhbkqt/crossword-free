package com.EnCrabStudio.crosswordfree;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.EnCrabStudio.crosswordfree.dialog.ChooserDialog;
import com.EnCrabStudio.crosswordfree.dialog.LanguageDialog;
import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;
import com.EnCrabStudio.crosswordfree.manager.AppNotificationManager;
import com.EnCrabStudio.crosswordfree.manager.InterstitialAdManager;
import com.EnCrabStudio.crosswordfree.manager.PushAdsManager;
import com.EnCrabStudio.crosswordfree.manager.SettingManager;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;
import com.EnCrabStudio.crosswordfree.model.Language;
import com.EnCrabStudio.crosswordfree.task.DownloadTask;
import com.EnCrabStudio.crosswordfree.task.WordImporterTask;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    private ProgressDialog mProgressDialog;
    private PushAdsManager mPushAdsManager;
    private SettingManager mSettingManager;
    private SoundManager mSoundManager;

    private Button btnChooseSize, btnChooseDifficulty;
    private View btnLanguage, btnPlay, btnContinue, btnSound, btnRate, btnShare, btnMore;
    private ImageView imvPushAdIcon;

    private int selectedSize;
    private int selectedDifficulty;

    private String[] difficulties;
    private String[] sizes = {"5 x 5", "7 x 7", "9 x 9", "11 x 11", "13 x 13", "15 x 15"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.appLaunchCounter(this);

        mSoundManager = SoundManager.getInstance();

        setContentView(R.layout.activity_main);

        difficulties = getResources().getStringArray(R.array.difficulties);

        initViews();
        importWords();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppNotificationManager.cancelScheduledAlarm(this);
        if (mPushAdsManager != null)
            mPushAdsManager.startPushIconDisplayer();
    }

    private void checkForUpdates() {

        if (mSettingManager == null)
            return;

        if (mSettingManager.shouldCheckUpdate(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light))
                    .setMessage(R.string.message_update)
                    .setPositiveButton(R.string.text_button_update, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AppUtils.openAppStoreLink(MainActivity.this);
                        }
                    })
                    .setNegativeButton(R.string.text_button_cancel, null)
                    .setCancelable(false);
            builder.show();
        }
    }

    @Override
    protected void onPause() {
        if (mPushAdsManager != null)
            mPushAdsManager.stopDisplayer();
        AppNotificationManager.scheduleNewAlarm(this);
        super.onPause();
    }

    private void importWords() {
        mProgressDialog = ProgressDialog.show(this, null, getString(R.string.loading));
        WordImporterTask importerTask = new WordImporterTask(this) {

            @Override
            public void onWordsImported() {
                mProgressDialog.dismiss();
                downloadJSON();
            }
        };
        importerTask.execute();
    }

    private void downloadJSON() {
        DownloadTask downloadTask = new DownloadTask(this) {
            @Override
            public void beforeDownloading() {
                mProgressDialog = ProgressDialog.show(MainActivity.this, null, getString(R.string.message_downloading));
            }

            @Override
            public void afterDownloading(String result) {
                mProgressDialog.dismiss();
                SettingManager.init(result);
                mSettingManager = SettingManager.getInstance();

                checkForUpdates();
                mPushAdsManager = new PushAdsManager(imvPushAdIcon);
                mPushAdsManager.startPushIconDisplayer();
                InterstitialAdManager.init(MainActivity.this);

                if (mSettingManager != null) {

                    boolean settingInitialized = SharedPreferenceUtil.getBoolean(MainActivity.this, Constants.KEY_SETTING_INITIALIZED, false);
                    if (!settingInitialized) {
                        SharedPreferenceUtil.putValue(MainActivity.this, Constants.KEY_GOOGLE_ADS_VISIBLE, mSettingManager.isPackageNameValid(MainActivity.this));
                    }
                    SharedPreferenceUtil.putValue(MainActivity.this, Constants.KEY_SETTING_INITIALIZED, true);
                }
            }
        };
        downloadTask.execute(Constants.JSON_CONFIG_FILE_URL);
    }

    private void initViews() {
        btnChooseSize = (Button) findViewById(R.id.btn_size);
        btnChooseDifficulty = (Button) findViewById(R.id.btn_difficulty);

        btnLanguage = findViewById(R.id.btn_language);
        btnSound = findViewById(R.id.btn_sound);
        btnRate = findViewById(R.id.btn_rate);
        btnShare = findViewById(R.id.btn_share);
        btnMore = findViewById(R.id.btn_more);

        btnPlay = findViewById(R.id.btn_play);
        btnContinue = findViewById(R.id.btn_continue);

        imvPushAdIcon = (ImageView) findViewById(R.id.imv_push_ad);

        btnChooseSize.setOnClickListener(this);
        btnChooseDifficulty.setOnClickListener(this);

        btnLanguage.setOnClickListener(this);
        btnSound.setOnClickListener(this);
        btnRate.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnMore.setOnClickListener(this);

        btnPlay.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        selectedSize = SharedPreferenceUtil.getInt(this, Constants.KEY_LAST_GAME_SIZE, 11);
        selectedDifficulty = SharedPreferenceUtil.getInt(this, Constants.KEY_LAST_GAME_DIFFICULT, 1);
        int sizeIndex = (selectedSize - 5) / 2;

        btnChooseDifficulty.setText(difficulties[selectedDifficulty]);
        btnChooseSize.setText(sizes[sizeIndex]);

        btnSound.setSelected(SoundManager.getInstance().isAppSoundEnabled());
    }

    private void chooseSize() {
        List<String> items = Arrays.asList(sizes);
        ChooserDialog dialog = new ChooserDialog(this, getString(R.string.title_choose_size), items, btnChooseSize.getText().toString(),
                new ChooserDialog.ChooserCallback() {
                    @Override
                    public void onChose(int selectedIndex) {
                        selectedSize = 5 + 2 * selectedIndex;
                        btnChooseSize.setText(sizes[selectedIndex]);
                    }
                });
        dialog.show();
    }

    private void chooseDifficulty() {

        List<String> items = Arrays.asList(difficulties);
        ChooserDialog dialog = new ChooserDialog(this, getString(R.string.title_choose_difficulty), items, btnChooseDifficulty.getText().toString(),
                new ChooserDialog.ChooserCallback() {
                    @Override
                    public void onChose(int selectedIndex) {
                        selectedDifficulty = selectedIndex;
                        btnChooseDifficulty.setText(difficulties[selectedIndex]);
                    }
                });
        dialog.show();
    }

    private void openNewGame() {

        // Save preferences
        SharedPreferenceUtil.putValue(this, Constants.KEY_LAST_GAME_DIFFICULT, selectedDifficulty);
        SharedPreferenceUtil.putValue(this, Constants.KEY_LAST_GAME_SIZE, selectedSize);

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(Constants.KEY_NEW_GAME, true);
        intent.putExtra(Constants.KEY_BOARD_SIZE, selectedSize);
        intent.putExtra(Constants.KEY_DIFFICULTY, selectedDifficulty);

        startActivityForResult(intent, Constants.REQUEST_PLAY_GAME);
    }

    private void continueLastGame() {

        boolean hasGameSaved = SharedPreferenceUtil.getBoolean(this, Constants.GAME_SAVED, false);
        if (hasGameSaved) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra(Constants.KEY_NEW_GAME, false);

            startActivityForResult(intent, Constants.REQUEST_PLAY_GAME);
        } else {
            Toast toast = Toast.makeText(this, R.string.message_no_saved_game, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void toggleSoundState() {
        btnSound.setSelected(!btnSound.isSelected());
        SoundManager.getInstance().setAppSoundEnabled(btnSound.isSelected());

        mSoundManager.playClickSound();
    }

    private void chooseLanguage() {
        LanguageDialog dialog = new LanguageDialog(this, new LanguageDialog.ChooserCallback() {
            @Override
            public void onChose(Language language) {
                AppUtils.openAppStoreLink(MainActivity.this, language.getPackageName());
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        if (v != btnSound) {
            mSoundManager.playClickSound();
        }

        if (v == btnChooseDifficulty) {
            chooseDifficulty();
        } else if (v == btnChooseSize) {
            chooseSize();
        } else if (v == btnPlay) {
            openNewGame();
        } else if (v == btnSound) {
            toggleSoundState();
        } else if (v == btnContinue) {
            continueLastGame();
        } else if (v == btnShare) {
            AppUtils.openShareIntent(this);
        } else if (v == btnMore) {
            AppUtils.openDeveloperLink(this);
        } else if (v == btnRate) {
            AppUtils.openAppStoreLink(this);
        } else if (v == btnLanguage) {
            chooseLanguage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_PLAY_GAME) {
            // InterstitialAdManager.getInstance().showInterstitial();
        }
    }
}

package com.EnCrabStudio.crosswordfree.model;

/**
 * Created by Admin on 10/3/2016
 */

public class AdItem {
    private String image;
    private String url;

    public AdItem(String image, String url) {
        this.image = image;
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPackageName() {
        String [] parts = url.split("=");
        return parts[1];
    }
}

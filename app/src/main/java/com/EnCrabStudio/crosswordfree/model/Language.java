package com.EnCrabStudio.crosswordfree.model;

/**
 * Created by Admin on 10/3/2016
 */

public class Language {
    private String packageName;
    private String displayName;
    private int drawableId;

    public Language(String packageName, String displayName, int drawableId) {
        this.packageName = packageName;
        this.displayName = displayName;
        this.drawableId = drawableId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}

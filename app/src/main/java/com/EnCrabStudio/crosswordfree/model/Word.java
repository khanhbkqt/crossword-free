package com.EnCrabStudio.crosswordfree.model;

/**
 * Created by Admin on 9/26/2016
 */
public class Word {
    private String word;
    private String hint;

    public Word(String word, String hint) {
        this.word = word;
        this.hint = hint;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    @Override
    public String toString() {
        return word;
    }
}

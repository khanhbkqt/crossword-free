package com.EnCrabStudio.crosswordfree.interfaces;

/**
 * Created by khanhnguyen on 12/10/2016
 */
public interface ICrosswordCellListener {
    void onCellClicked(int position);
}

package com.EnCrabStudio.crosswordfree.interfaces;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public interface ICrosswordSound {
    void playClickSound();
    void playWinSound();
    void playLoseSound();
}

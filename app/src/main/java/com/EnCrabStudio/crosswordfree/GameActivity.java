package com.EnCrabStudio.crosswordfree;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.EnCrabStudio.crosswordfree.crossword.Generator;
import com.EnCrabStudio.crosswordfree.crossword.Grid;
import com.EnCrabStudio.crosswordfree.crossword.WordCoordinate;
import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.RandomUtils;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;
import com.EnCrabStudio.crosswordfree.helper.SpacesItemDecoration;
import com.EnCrabStudio.crosswordfree.interfaces.ICrosswordCellListener;
import com.EnCrabStudio.crosswordfree.manager.AppNotificationManager;
import com.EnCrabStudio.crosswordfree.manager.GameManager;
import com.EnCrabStudio.crosswordfree.manager.InterstitialAdManager;
import com.EnCrabStudio.crosswordfree.manager.RatingManager;
import com.EnCrabStudio.crosswordfree.manager.SettingManager;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;
import com.EnCrabStudio.crosswordfree.manager.WordManager;
import com.EnCrabStudio.crosswordfree.view.CrosswordAdapter;
import com.EnCrabStudio.crosswordfree.view.CrosswordGridAdapter;
import com.EnCrabStudio.crosswordfree.view.KeyboardAdapter;
import com.EnCrabStudio.crosswordfree.dialog.MenuDialog;
import com.EnCrabStudio.crosswordfree.view.QwertyKeyboard;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by khanhnguyen on 26/09/2016
 */
public class GameActivity extends Activity implements View.OnClickListener,
        AdapterView.OnItemClickListener, MenuDialog.MenuDialogCallback, QwertyKeyboard.QwertyKeyboardCallback, ICrosswordCellListener {

    private static final long BANNER_SHOW_DELAY = 60000;

    private Map<String, String> words;

    private ProgressDialog mProgressDialog;
    private GridView mKeyboardGrid;
    private RecyclerView mRecyclerView;
    private TextView mHintTextView;
    private View mInfoView;
    private TextView mStatusTextView;
    private View mPlayButton, mHintButton;

    private AdView adView;
    private Timer timer = new Timer();

    private GameManager mGameManager;
    private SettingManager mSettingManager;

    private QwertyKeyboard mQwertyKeyboard;
    private Grid mCrosswordGrid;
    private CrosswordGridAdapter mCrosswordAdapter;
    private KeyboardAdapter mKeyboardAdapter;
    private Hashtable<WordCoordinate, String> wordCoordinateHashtable;
    private int difficulty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sendGoogleAnalytic();
        mGameManager = GameManager.getInstance();
        mSettingManager = SettingManager.getInstance();

        InterstitialAdManager.getInstance().showInterstitial();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        WordManager wordManager = WordManager.getInstance(this);
        words = wordManager.getAllWords();

        setContentView(R.layout.activity_home);

        mKeyboardGrid = (GridView) findViewById(R.id.keyboard_grid);
        mHintTextView = (TextView) findViewById(R.id.tv_hint);
        adView = (AdView) findViewById(R.id.adView);
        mInfoView = findViewById(R.id.notification_view);
        mStatusTextView = (TextView) findViewById(R.id.tv_status);
        mHintButton = findViewById(R.id.btn_hint);
        mPlayButton = findViewById(R.id.btn_play);
        mRecyclerView = (RecyclerView) findViewById(R.id.crossword_recycler_view);

        mQwertyKeyboard = (QwertyKeyboard) findViewById(R.id.qwerty_keyboard);
        mQwertyKeyboard.setKeyboardCallback(this);

        mHintButton.setOnClickListener(this);
        mPlayButton.setOnClickListener(this);

        mKeyboardGrid.setNumColumns(6);
        mKeyboardGrid.setFadingEdgeLength(0);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(1));

        ((SimpleItemAnimator) mRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        if (mSettingManager == null || mSettingManager.canShowGoogleAds(this)) {
            adView.setVisibility(View.VISIBLE);
            timer.scheduleAtFixedRate(new ShowAdTask(), 0, BANNER_SHOW_DELAY);
        } else {
            adView.setVisibility(View.GONE);
        }

        Intent intent = getIntent();
        if (intent != null && intent.getBooleanExtra(Constants.KEY_NEW_GAME, false)) {
            generateGame(intent);
        } else if (mGameManager.restoreGameState(this)) {
            mCrosswordAdapter = mGameManager.getGameGridAdapter();
            wordCoordinateHashtable = mGameManager.getWordCoordinateHashtable();

            mCrosswordGrid = mCrosswordAdapter.getOriginalGrid();
            difficulty = mGameManager.getDifficulty();

            mRecyclerView.setLayoutManager(new GridLayoutManager(this, mCrosswordGrid.getMaxColumns()));
            mRecyclerView.setAdapter(mCrosswordAdapter);
            mCrosswordAdapter.setCellClickListener(this);

            mHintTextView.setText(R.string.text_touch_a_square);

            if (difficulty == 2) {
                mQwertyKeyboard.init();
                mQwertyKeyboard.setVisibility(View.VISIBLE);
                mKeyboardGrid.setVisibility(View.GONE);
            } else {
                mKeyboardAdapter = new KeyboardAdapter();
                mKeyboardAdapter.setCharacters(new ArrayList<Character>());
                mKeyboardGrid.setAdapter(mKeyboardAdapter);
                mKeyboardGrid.setOnItemClickListener(GameActivity.this);

                mQwertyKeyboard.setVisibility(View.GONE);
                mKeyboardGrid.setVisibility(View.VISIBLE);
            }
        } else {
            Toast toast = Toast.makeText(this, R.string.message_error_loading_saved_game, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            SharedPreferenceUtil.putValue(this, Constants.GAME_SAVED, false);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppNotificationManager.cancelScheduledAlarm(this);
    }

    @Override
    protected void onPause() {
        AppNotificationManager.scheduleNewAlarm(this);
        super.onPause();
    }

    private void showMenu() {
        MenuDialog dialog = new MenuDialog(this, this);
        dialog.show();
    }

    private void sendGoogleAnalytic() {
        CrosswordFreeApplication application = (CrosswordFreeApplication) getApplication();
        Tracker tracker = application.getDefaultTracker();

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory("Game Play")
                .setAction("New Game")
                .setLabel("New Game created")
                .setValue(1)
                .build());
    }

    private void generateGame(Intent intent) {
        if (intent.getBooleanExtra(Constants.KEY_NEW_GAME, false)) {
            int size = intent.getIntExtra(Constants.KEY_BOARD_SIZE, 5);
            difficulty = intent.getIntExtra(Constants.KEY_DIFFICULTY, 0);

            new GeneratorTask().execute(size, difficulty);
        }
    }

    private void createCrosswordBoard(int row, int col) {
        mCrosswordGrid = new Grid(row, col);

        List<String> allWords = new ArrayList<>(words.keySet());
        Generator gen = new Generator(mCrosswordGrid, allWords);

        long startTime = System.currentTimeMillis();
        gen.generate();
        long operateTime = System.currentTimeMillis() - startTime;

        System.out.println("Crossword created in " + operateTime + "ms");

        mCrosswordGrid.show();

        wordCoordinateHashtable = gen.getWordCoordinateTable();
    }

    private void onKeyboardPressed(int position) {
        char selectedChar = mKeyboardAdapter.getItem(position);
        onKeyPressed(selectedChar);
    }

    @Override
    public void onKeyPressed(char selectedChar) {
        if (selectedChar == Constants.CHAR_DELETE) {
            mCrosswordAdapter.delete();
            SoundManager.getInstance().playClickSound();

            GameManager.getInstance().saveGameState(GameActivity.this);
            SharedPreferenceUtil.putValue(this, Constants.GAME_SAVED, true);

        } else if (selectedChar == Constants.CHAR_HINT) {
            showMenu();
            SoundManager.getInstance().playClickSound();
        } else if (selectedChar != Constants.CHAR_NOTHING) {

            mCrosswordAdapter.putCharacter(selectedChar);
            SoundManager.getInstance().playClickSound();

            GameManager.getInstance().saveGameState(GameActivity.this);
            SharedPreferenceUtil.putValue(this, Constants.GAME_SAVED, true);

            checkGameFinished();
        }
    }

    private void checkGameFinished() {
        if (mCrosswordAdapter.isGameFinished()) {

            SharedPreferenceUtil.putValue(this, Constants.GAME_SAVED, false);

            mCrosswordAdapter.setCellClickListener(null);
            mKeyboardGrid.setOnItemClickListener(null);

            if (difficulty == 2) {
                mKeyboardGrid.setVisibility(View.GONE);
                mQwertyKeyboard.setVisibility(View.INVISIBLE);
            } else {
                mKeyboardGrid.setVisibility(View.INVISIBLE);
                mQwertyKeyboard.setVisibility(View.GONE);
            }

            mInfoView.setVisibility(View.VISIBLE);

            if (mCrosswordAdapter.isPlayerWon()) {
                mStatusTextView.setText(R.string.text_you_win);
                SoundManager.getInstance().playWinSound();
                RatingManager.getInstance().showRatingReminderIfNeeded(this);
            } else {
                SoundManager.getInstance().playLoseSound();
                mStatusTextView.setText(R.string.text_you_lose);
            }
        }
    }

    @Override
    public void onCellClicked(int position) {
        List<WordCoordinate> coordinates = new ArrayList<>();
        for (WordCoordinate coordinate : wordCoordinateHashtable.keySet()) {
            if (coordinate.inCoordinate(position, mCrosswordGrid, wordCoordinateHashtable.get(coordinate))) {
                coordinates.add(coordinate);
            }
        }

        if (position == mCrosswordAdapter.getSelectedPosition() && coordinates.size() == 2) {
            WordCoordinate coordinate = mCrosswordAdapter.getCurrentWordCoordinate();
            if (coordinates.indexOf(coordinate) == 0)
                coordinate = coordinates.get(1);
            else
                coordinate = coordinates.get(0);

            mCrosswordAdapter.switchWord(coordinate, wordCoordinateHashtable.get(coordinate), position);

            notifyKeyboard();
            SoundManager.getInstance().playClickSound();

        } else if (!coordinates.isEmpty()) {

            mCrosswordAdapter.switchWord(coordinates.get(0), wordCoordinateHashtable.get(coordinates.get(0)), position);

            notifyKeyboard();
            SoundManager.getInstance().playClickSound();
        }

        if (mCrosswordAdapter.getCurrentWord() != null) {
            mHintTextView.setText(getHintByWord(mCrosswordAdapter.getCurrentWord()));
        }
    }

    @Override
    public void onBackPressed() {
        showMenu();
    }

    private void notifyKeyboard() {

        String allCharacters = getString(R.string.characters);

        if (difficulty < 2) {
            int size = difficulty == 1 ? 10 : mCrosswordAdapter.getCurrentWord().length();
            List<Character> chars = RandomUtils.randomCharArray(allCharacters, size, mCrosswordAdapter.getCurrentWord());

            mKeyboardAdapter.setCharacters(chars);
            mKeyboardAdapter.notifyDataSetChanged();
        } else {
            mQwertyKeyboard.setKeyBoardEnabled(true);
        }
    }

    private void loadBannerAd() {
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
    }

    private String getHintByWord(String word) {
        return words.get(word);
    }

    @Override
    public void onClick(View v) {

        SoundManager.getInstance().playClickSound();

        if (v == mPlayButton) {
            new GeneratorTask().execute(mCrosswordGrid.getMaxColumns());
            InterstitialAdManager.getInstance().showInterstitial();
        } else if (v == mHintButton) {
            showMenu();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent == mKeyboardGrid) {
            onKeyboardPressed(position);
        }
    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        super.onDestroy();
    }

    @Override
    public void onHintButtonClicked() {
        if (mCrosswordAdapter.getCurrentWord() != null) {
            mCrosswordAdapter.showCurrentWord();
            checkGameFinished();
            mGameManager.saveGameState(this);
        } else {
            Toast.makeText(this, getString(R.string.text_touch_a_square), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSolveButtonClicked() {

        SharedPreferenceUtil.putValue(this, Constants.GAME_SAVED, false);

        mCrosswordAdapter.showAnswer();
        SoundManager.getInstance().playLoseSound();

        mQwertyKeyboard.setKeyBoardEnabled(false);
        mCrosswordAdapter.setCellClickListener(null);
        mKeyboardGrid.setOnItemClickListener(null);
        mKeyboardGrid.setVisibility(View.INVISIBLE);
        mQwertyKeyboard.setVisibility(View.INVISIBLE);
        mInfoView.setVisibility(View.VISIBLE);

        mStatusTextView.setText(R.string.text_you_lose);
    }

    @Override
    public void onBackButtonClicked() {
        // nothing to do
    }

    @Override
    public void onExitButtonClicked() {
        finish();
    }

    class ShowAdTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadBannerAd();
                }
            });
        }
    }

    class GeneratorTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(GameActivity.this, null, getString(R.string.text_generating_crossword));
        }

        @Override
        protected Void doInBackground(Integer... params) {
            int size = params[0];
            createCrosswordBoard(size, size);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            mRecyclerView.setLayoutManager(new GridLayoutManager(GameActivity.this, mCrosswordGrid.getMaxColumns()));

            mCrosswordAdapter = new CrosswordGridAdapter(mCrosswordGrid);
            mRecyclerView.setAdapter(mCrosswordAdapter);

            mCrosswordAdapter.setCellClickListener(GameActivity.this);
            mKeyboardGrid.setOnItemClickListener(GameActivity.this);

            mRecyclerView.setLayoutManager(new GridLayoutManager(GameActivity.this, mCrosswordGrid.getMaxColumns()));

            mGameManager.newGame(mCrosswordGrid.getMaxColumns(), difficulty, mCrosswordAdapter, wordCoordinateHashtable);

            if (difficulty < 2) {
                mKeyboardGrid.setVisibility(View.VISIBLE);
                mQwertyKeyboard.setVisibility(View.GONE);

                mKeyboardAdapter = new KeyboardAdapter();
                mKeyboardAdapter.setCharacters(new ArrayList<Character>());
                mKeyboardGrid.setAdapter(mKeyboardAdapter);
            } else {
                mQwertyKeyboard.init();
                mQwertyKeyboard.setVisibility(View.VISIBLE);
                mKeyboardGrid.setVisibility(View.GONE);
            }

            mInfoView.setVisibility(View.INVISIBLE);
            mHintTextView.setText(R.string.text_touch_a_square);

            GameManager.getInstance().saveGameState(GameActivity.this);
            SharedPreferenceUtil.putValue(GameActivity.this, Constants.GAME_SAVED, true);

            mProgressDialog.dismiss();
        }
    }
}

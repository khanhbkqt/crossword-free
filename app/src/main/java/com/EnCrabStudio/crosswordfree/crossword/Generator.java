package com.EnCrabStudio.crosswordfree.crossword;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

/**
 * Created by khanhnguyen on 27/09/2016
 */
public class Generator {

    private static final int MAX_COMPUTE_TIME = 5000;

    private Grid grid;
    private List<String> words, tempWords;

    private Hashtable<Integer, List<String>> horizontalAnnex;
    private Hashtable<Integer, List<String>> verticalAnnex;
    private Hashtable<WordCoordinate, String> wordCoordinateTable;

    private Random rnd;

    public Generator(Grid grid, List<String> words) {
        this.grid = grid;
        this.words = words;

        rnd = new Random();

        orderWords();

        horizontalAnnex = new Hashtable<>();
        verticalAnnex = new Hashtable<>();
    }

    private void orderWords() {
        Collections.shuffle(words);
        tempWords = new ArrayList<>();

        int numWordsNeeded = Math.min(words.size(), 25 * (16 - grid.getMaxColumns() + 1) / 2);

        for (int i = 0; tempWords.size() < numWordsNeeded && i < words.size(); i++) {
            String word = words.get(i);
            if (word.length() <= grid.getMaxColumns())
                tempWords.add(words.get(i));
        }
        Collections.sort(tempWords, new StringLengthComparator());
    }

    public int generate() {

        long startTime = System.currentTimeMillis();

        List<String> remainingWords = new ArrayList<>();
        for (String word : tempWords)
            remainingWords.add(word);

        //initialization
        String word = remainingWords.get(0);
        int direction = rnd.nextInt(2);

        WordCoordinate coordinate;
        if (direction == Grid.ORIENTATION_HORIZONTAL)
            coordinate = new WordCoordinate(rnd.nextInt(grid.getMaxColumns()), 0, direction, 0);
        else
            coordinate = new WordCoordinate(0, rnd.nextInt(grid.getMaxColumns()), direction, 0);

        while (word != null && System.currentTimeMillis() - startTime < MAX_COMPUTE_TIME) {

            writeWordToGrid(word, coordinate);
            remainingWords.remove(word);

            addToAnnex(word, coordinate);
            addToWordCoordinateTable(word, coordinate);

            Object[] result = pickupWord(remainingWords);
            word = (String) result[0];
            coordinate = (WordCoordinate) result[1];
        }

        return words.size() - remainingWords.size();
    }

    private Object[] pickupWord(List<String> remainingWords) {
        WordCoordinate maxCoordinate = null;
        String theWord = null;

        for (String word : remainingWords) {

            List<WordCoordinate> coordinates = getPossibleCoordinates(word);

            if (coordinates.size() == 0) continue;

            Collections.sort(coordinates, new WordCoordinateComparator());

            if (maxCoordinate == null || (maxCoordinate.getScore() < coordinates.get(0).getScore())) {

                maxCoordinate = coordinates.get(0);
                theWord = word;
            }
        }

        return new Object[]{theWord, maxCoordinate};
    }

    private List<WordCoordinate> getPossibleCoordinates(String word) {

        List<WordCoordinate> coordList = new ArrayList<>();

        for (int k = 0; k < word.length(); k++) {

            for (int i = 0; i < grid.getMaxRows(); i++) {

                for (int j = 0; j < grid.getMaxColumns(); j++) {

                    //horizontal
                    if (j - k >= 0 && ((j - k) + word.length() < grid.getMaxColumns()))
                        coordList.add(new WordCoordinate(i, j - k, 0, 0));

                    //vertical
                    if (i - k >= 0 && ((i - k) + word.length() < grid.getMaxRows()))
                        coordList.add(new WordCoordinate(i - k, j, 1, 0));
                }
            }
        }

        List<WordCoordinate> toRet = new ArrayList<>();

        for (WordCoordinate coordinate : coordList) {

            int score = checkScore(word, coordinate);
            if (score == 0) continue;

            toRet.add(new WordCoordinate(coordinate.getX(), coordinate.getY(), coordinate.getDirection(), score));
        }

        return toRet;
    }

    private int checkScore(String word, WordCoordinate coordinate) {
        int row = coordinate.getX();
        int col = coordinate.getY();

        if (col < 0 || row < 0)
            return 0;

        int count = 1;
        int score = 1;

        for (Character letter : word.toCharArray()) {

            if (!isEmpty(row, col) && grid.getValue(row, col) != letter)
                return 0;

            if (grid.getValue(row, col) == letter)
                score++;

            if (coordinate.getDirection() == 1) {
                //vertical
                if (grid.getValue(row, col) != letter && (!isEmpty(row, col + 1) || !isEmpty(row, col - 1)))
                    return 0;

                if (count == 1 && !isEmpty(row - 1, col))
                    return 0;

                if (count == word.length() && !isEmpty(row + 1, col))
                    return 0;

                row++;

            } else {
                //Horizontal

                if (grid.getValue(row, col) != letter && (!isEmpty(row - 1, col) || !isEmpty(row + 1, col)))
                    return 0;

                if (count == 1 && !isEmpty(row, col - 1))
                    return 0;

                if (count == word.length() && !isEmpty(row, col + 1))
                    return 0;

                col++;
            }

            count++;
        }

        return score;
    }

    private boolean isEmpty(int row, int column) {

        char value = grid.getValue(row, column);

        return value == ' ' || value == 0;
    }

    private void writeWordToGrid(String word, WordCoordinate coordinate) {

        int k = 0;

        for (int i = (coordinate.getDirection() == 0) ? coordinate.getY() : coordinate.getX();
             i < word.length() + ((coordinate.getDirection() == 0) ? coordinate.getY() : coordinate.getX());
             i++)
            grid.setValue(coordinate.getDirection() == 0 ? coordinate.getX() : i,
                    coordinate.getDirection() == 0 ? i : coordinate.getY(), word.charAt(k++));

    }

    private void addToAnnex(String word, WordCoordinate coordinate) {
        if (coordinate.getDirection() == 0) {
            List<String> l = horizontalAnnex.get(coordinate.getX());
            if (l == null) l = new ArrayList<>();
            l.add(word);
            horizontalAnnex.put(coordinate.getX(), l);
        } else {

            List<String> l = verticalAnnex.get(coordinate.getY());
            if (l == null) l = new ArrayList<>();
            l.add(word);
            verticalAnnex.put(coordinate.getY(), l);
        }
    }

    private void addToWordCoordinateTable(String word, WordCoordinate coordinate) {
        if (wordCoordinateTable == null) {
            wordCoordinateTable = new Hashtable<>();
        }
        wordCoordinateTable.put(coordinate, word);
    }

    public Hashtable<Integer, List<String>> getHorizontalAnnex() {
        return horizontalAnnex;
    }

    public Hashtable<Integer, List<String>> getVerticalAnnex() {
        return verticalAnnex;
    }

    public Hashtable<WordCoordinate, String> getWordCoordinateTable() {
        return wordCoordinateTable;
    }
}

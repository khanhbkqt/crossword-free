package com.EnCrabStudio.crosswordfree.crossword;

/**
 * Created by khanhnguyen on 27/09/2016
 */
public class Grid {

    public static final int ORIENTATION_NONE = -1;
    public static final int ORIENTATION_VERTICAL = 1;
    public static final int ORIENTATION_HORIZONTAL = 0;

    private char[][] matrix;
    private int maxRows;
    private int maxColumns;

    public Grid(int maxRows, int maxColumns){
        this.maxRows = maxRows;
        this.maxColumns = maxColumns;

        matrix = new char[maxRows][maxColumns];

        initialize();
    }

    public int setValue(int row, int column, char val){

        if(row >= maxRows | column >= maxColumns | row < 0 | column < 0) return -1;

        matrix[row][column] = val;

        return row * maxColumns + column;
    }

    public char getValue(int row, int column){

        if(row >= maxRows | column >= maxColumns | row < 0 | column < 0)return 0;

        return matrix[row][column];
    }

    public int getMaxRows() {
        return maxRows;
    }

    public int getMaxColumns() {
        return maxColumns;
    }

    public void initialize(){

        for(int i = 0; i < maxRows; i++)
            for(int j = 0; j < maxColumns; j++)
                matrix[i][j] = ' ';
    }

    public void show() {

        for (int j = 0; j < maxColumns; j++)
            System.out.print("\t" + j);

        System.out.println();

        for (int i = 0; i < maxRows; i++) {
            System.out.print(i + "\t");

            for (int j = 0; j < maxColumns; j++)
                System.out.print((matrix[i][j] == ' ' ? '-' : matrix[i][j] )+ "\t");

            System.out.println();
        }
    }

    public int[] getWordPostions(WordCoordinate coordinate, String word) {
        int[] position = new int[word.length()];
        if (coordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            for (int i = 0; i < word.length(); i++) {
                position[i] = coordinate.getX() * maxColumns + coordinate.getY() + i;
            }
        } else {
            for (int i = 0; i < word.length(); i++) {
                position[i] = (coordinate.getX() + i) * maxColumns + coordinate.getY();
            }
        }
        return position;
    }

    public Grid cloneForPlaying() {
        Grid grid = new Grid(maxRows, maxColumns);
        for (int row = 0; row < maxRows; row++) {
            for (int col = 0; col < maxColumns; col++) {
                if (getValue(row, col) != ' ') {
                    grid.setValue(row, col, '_');
                } else {
                    grid.setValue(row, col, ' ');
                }
            }
        }
        return grid;
    }
}

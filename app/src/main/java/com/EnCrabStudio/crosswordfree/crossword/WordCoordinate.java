package com.EnCrabStudio.crosswordfree.crossword;

import java.io.Serializable;

/**
 * Created by khanhnguyen on 27/09/2016
 */
public class WordCoordinate implements Serializable {

    private int x;
    private int y;
    private int score;

    public WordCoordinate(int x, int y, int direction, int score) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.score = score;
    }

    private int direction; // 0 => hor, 1 => ver

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDirection() {
        return direction;
    }

    public int getScore() {
        return score;
    }

    public boolean inCoordinate(int position, Grid grid, String word) {
        int itemCol = position % grid.getMaxColumns();
        int itemRow = position / grid.getMaxColumns();

        if (getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            return (itemRow == x) && (itemCol < y + word.length()) && (itemCol >= y);
        } else {
            return (itemCol == y) && (itemRow < x + word.length()) && (itemRow >= x);
        }
    }
}

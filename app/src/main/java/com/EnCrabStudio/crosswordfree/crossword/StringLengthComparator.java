package com.EnCrabStudio.crosswordfree.crossword;

import java.util.Comparator;

/**
 * Created by khanhnguyen on 27/09/2016
 */
public class StringLengthComparator implements Comparator<String> {

    @Override
    public int compare(String s1, String s2) {
        return s2.length() - s1.length();
    }
}

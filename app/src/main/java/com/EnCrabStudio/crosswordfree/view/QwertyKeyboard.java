package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.helper.Constants;

import java.util.HashMap;

/**
 * Created by Admin on 10/4/2016
 */

public class QwertyKeyboard extends LinearLayout implements View.OnClickListener {

    private QwertyKeyboardCallback mKeyboardCallback;
    private HashMap<View, Character> mKeyDictionary;
    private String allChars = "";

    public QwertyKeyboard(Context context) {
        super(context);
        init();
    }

    public QwertyKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public QwertyKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setKeyboardCallback(QwertyKeyboardCallback keyboardCallback) {
        this.mKeyboardCallback = keyboardCallback;
    }

    public void init() {
        mKeyDictionary = new HashMap<>();
        removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.layout_qwerty_keyboard, this, true);
    }

    public void setKeyBoardEnabled(boolean enabled) {
        OnClickListener listener = enabled ? this : null;
        for (View key : mKeyDictionary.keySet()) {
            key.setOnClickListener(listener);
            key.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);

        if (child instanceof ViewGroup) {
            ViewGroup keyboard = (ViewGroup) child;
            for (int k = 0; k < keyboard.getChildCount(); k++) {
                LinearLayout keyLine = (LinearLayout) keyboard.getChildAt(k);
                for (int i = 0; i < keyLine.getChildCount(); i++) {
                    View keyView = keyLine.getChildAt(i);
                    keyView.setOnClickListener(this);
                    keyView.setVisibility(INVISIBLE);

                    if (keyView instanceof AutoResizeSquareTextView) {
                        mKeyDictionary.put(keyView, ((TextView) keyView).getText().charAt(0));
                        allChars += ((TextView) keyView).getText();
                    } else if (keyView.getId() == R.id.key_hint) {
                        mKeyDictionary.put(keyView, Constants.CHAR_HINT);
                        keyView.setVisibility(VISIBLE);
                    } else if (keyView.getId() == R.id.key_delete) {
                        mKeyDictionary.put(keyView, Constants.CHAR_DELETE);
                    }
                }
            }
        }

        System.out.println("All chars = " + allChars);
    }

    @Override
    public void onClick(View v) {
        Character key = mKeyDictionary.get(v);
        if (key != null) {
            mKeyboardCallback.onKeyPressed(key);
        }
    }

    public interface QwertyKeyboardCallback {
        void onKeyPressed(char key);
    }
}

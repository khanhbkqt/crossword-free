package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.manager.TypefaceManager;

/**
 * Created by khanhnguyen on 12/10/2016
 */
public class MyriadButton extends Button {
    public MyriadButton(Context context) {
        super(context);
        init();
    }

    public MyriadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyriadButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(TypefaceManager.MYRIAD);
    }
}

package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.crossword.Grid;
import com.EnCrabStudio.crosswordfree.crossword.WordCoordinate;

/**
 * Created by Admin on 9/27/2016
 */
public class CrosswordAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private Grid mOriginalGrid, mPlayingGrid;
    private WordCoordinate mCurrentWordCoordinate;
    private String currentWord;
    private int selectedPosition = -1;

    public CrosswordAdapter(Grid grid) {
        this.mOriginalGrid = grid;
        this.mPlayingGrid = grid.cloneForPlaying();
    }

    public CrosswordAdapter(Grid originalGird, Grid playerGird) {
        this.mOriginalGrid = originalGird;
        this.mPlayingGrid = playerGird;
    }

    @Override
    public int getCount() {
        return mOriginalGrid.getMaxColumns() * mOriginalGrid.getMaxRows();
    }

    @Override
    public Character getItem(int position) {
        int itemCol = position % mOriginalGrid.getMaxColumns();
        int itemRow = position / mOriginalGrid.getMaxColumns();
        return mPlayingGrid.getValue(itemRow, itemCol);
    }

    public Character getAbsoluteItem(int position) {
        int itemCol = position % mOriginalGrid.getMaxColumns();
        int itemRow = position / mOriginalGrid.getMaxColumns();
        return mOriginalGrid.getValue(itemRow, itemCol);
    }

    public void setCurrentWordCoordinate(WordCoordinate mCurrentWordCoordinate) {
        this.mCurrentWordCoordinate = mCurrentWordCoordinate;
    }

    public WordCoordinate getCurrentWordCoordinate() {
        return mCurrentWordCoordinate;
    }

    public Grid getOriginalGrid() {
        return mOriginalGrid;
    }

    public Grid getPlayingGrid() {
        return mPlayingGrid;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mLayoutInflater == null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = mLayoutInflater.inflate(R.layout.cell_crossword, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.bind(position);
        return view;
    }

    public String getCurrentWord() {
        return currentWord;
    }

    /**
     * Put character to current selecting word
     *
     * @param c character
     * @return true if selectedPositionChanged -> change Keyboard
     */
    public boolean putCharacter(char c) {

        boolean positionChanged = false;

        int itemCol = selectedPosition % mOriginalGrid.getMaxColumns();
        int itemRow = selectedPosition / mOriginalGrid.getMaxColumns();
        mPlayingGrid.setValue(itemRow, itemCol, c);

        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            if (mCurrentWordCoordinate.inCoordinate(selectedPosition + 1, mOriginalGrid, currentWord)) {
                selectedPosition++;
                positionChanged = true;
            }
        } else {
            int newPosition = (itemRow + 1) * mOriginalGrid.getMaxRows() + itemCol;
            if (mCurrentWordCoordinate.inCoordinate(newPosition, mOriginalGrid, currentWord)) {
                selectedPosition = newPosition;
                positionChanged = true;
            }
        }

        notifyDataSetChanged();
        return positionChanged;
    }

    public boolean delete() {
        boolean positionChanged = false;

        int itemCol = selectedPosition % mOriginalGrid.getMaxColumns();
        int itemRow = selectedPosition / mOriginalGrid.getMaxColumns();
        mPlayingGrid.setValue(itemRow, itemCol, '_');

        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            if (mCurrentWordCoordinate.inCoordinate(selectedPosition - 1, mOriginalGrid, currentWord)) {
                selectedPosition--;
                positionChanged = true;
            }
        } else {
            int newPosition = (itemRow - 1) * mOriginalGrid.getMaxRows() + itemCol;
            if (mCurrentWordCoordinate.inCoordinate(newPosition, mOriginalGrid, currentWord)) {
                selectedPosition = newPosition;
                positionChanged = true;
            }
        }

        notifyDataSetChanged();
        return positionChanged;
    }

    public boolean isGameFinished() {
        for (int row = 0; row < mOriginalGrid.getMaxRows(); row++) {
            for (int col = 0; col < mOriginalGrid.getMaxColumns(); col++) {
                if (mPlayingGrid.getValue(row, col) == '_') {
                    return false;
                }
            }
        }
        selectedPosition = -1;
        currentWord = null;
        mCurrentWordCoordinate = null;
        return true;
    }

    public boolean isPlayerWon() {
        for (int row = 0; row < mOriginalGrid.getMaxRows(); row++) {
            for (int col = 0; col < mOriginalGrid.getMaxRows(); col++) {
                if (mPlayingGrid.getValue(row, col) != mOriginalGrid.getValue(row, col)) {
                    return false;
                }
            }
        }
        return true;
    }

    public void showCurrentWord() {
        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            for (int columnIndex = 0; columnIndex < currentWord.length(); columnIndex++) {
                mPlayingGrid.setValue(mCurrentWordCoordinate.getX(), mCurrentWordCoordinate.getY() + columnIndex, mOriginalGrid.getValue(mCurrentWordCoordinate.getX(), mCurrentWordCoordinate.getY() + columnIndex));
            }
        } else {
            for (int rowIndex = 0; rowIndex < currentWord.length(); rowIndex++) {
                mPlayingGrid.setValue(mCurrentWordCoordinate.getX() + rowIndex, mCurrentWordCoordinate.getY(), mOriginalGrid.getValue(mCurrentWordCoordinate.getX() + rowIndex, mCurrentWordCoordinate.getY()));
            }
        }
        notifyDataSetChanged();
    }

    public void showAnswer() {
        this.mPlayingGrid = mOriginalGrid;
        notifyDataSetChanged();
    }

    class ViewHolder {

        private int adapterPosition;

        private View background;
        private TextView character;
        private Context context;

        private int colorCellNormal, colorCellSelected;

        public ViewHolder(View view) {
            background = view.findViewById(R.id.cell_background);
            character = (TextView) view.findViewById(R.id.cell_character);
            context = background.getContext();

            colorCellNormal = ContextCompat.getColor(context, R.color.cell_normal);
            colorCellSelected = ContextCompat.getColor(context, R.color.cell_selected);
        }

        public void bind(int position) {

            this.adapterPosition = position;
            char c = getItem(position);

            if (c == ' ') {
                background.setBackgroundColor(Color.BLACK);
            } else {
                background.setBackgroundColor(Color.WHITE);
                if (c != '_') {
                    if (c != 'ß') {
                        character.setText(Character.toString(c).toUpperCase());
                    } else {
                        character.setText(String.valueOf(c));
                    }
                } else {
                    character.setText("");
                }
            }

            if (getAdapterPosition() == selectedPosition) {
                background.setBackgroundColor(colorCellSelected);
            } else if (mCurrentWordCoordinate != null &&
                    mCurrentWordCoordinate.inCoordinate(getAdapterPosition(), mOriginalGrid, currentWord)) {
                background.setBackgroundColor(colorCellNormal);
            }
        }

        public int getAdapterPosition() {
            return adapterPosition;
        }
    }
}

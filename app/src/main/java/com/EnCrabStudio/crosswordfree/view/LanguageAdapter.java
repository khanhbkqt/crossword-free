package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.model.Language;

import java.util.List;

/**
 * Created by Admin on 10/3/2016
 */

public class LanguageAdapter extends BaseAdapter {

    private Context context;
    private List<Language> languages;

    public LanguageAdapter(Context context) {
        this.context = context;
        this.languages = AppUtils.loadLanguageList(context);
    }

    @Override
    public int getCount() {
        return languages.size();
    }

    @Override
    public Language getItem(int position) {
        return languages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            view = inflater.inflate(R.layout.adapter_language, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Language language = getItem(position);
        holder.bind(language);

        return view;
    }

    class ViewHolder {
        private String currentPackageName = context.getPackageName();
        private ImageView imvFlag, imvTick;
        private TextView tvDisplayName;

        public ViewHolder(View itemView) {
            imvFlag = (ImageView) itemView.findViewById(R.id.imv_language_flag);
            imvTick = (ImageView) itemView.findViewById(R.id.imv_tick);
            tvDisplayName = (TextView) itemView.findViewById(R.id.tv_display_name);
        }

        public void bind(Language language) {
            imvFlag.setImageResource(language.getDrawableId());
            tvDisplayName.setText(language.getDisplayName());
            imvTick.setVisibility(currentPackageName.equals(language.getPackageName()) ? View.VISIBLE : View.GONE);
        }
    }
}

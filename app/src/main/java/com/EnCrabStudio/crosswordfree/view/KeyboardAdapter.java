package com.EnCrabStudio.crosswordfree.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.helper.Constants;

import java.util.List;

/**
 * Created by Admin on 9/27/2016
 */
public class KeyboardAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private List<Character> characters;
    private boolean shouldShowDeleteButton = false;

    public KeyboardAdapter() {
    }

    public void setCharacters(List<Character> characters) {

        shouldShowDeleteButton = !characters.isEmpty();

        this.characters = characters;
        while (characters.size() < 10) {
            characters.add(Constants.CHAR_NOTHING);
        }
        characters.add(5, Constants.CHAR_DELETE); // Delete
        characters.add(11, Constants.CHAR_HINT); // Hint
    }

    @Override
    public int getCount() {
        return characters == null ? 0 : characters.size();
    }

    @Override
    public Character getItem(int position) {
        return characters.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mLayoutInflater == null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = mLayoutInflater.inflate(R.layout.cell_keyboard, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.bind(position);
        return view;
    }

    class ViewHolder {

        private View itemView;
        private TextView character;
        private View keyBackground;

        public ViewHolder(View view) {
            itemView = view;
            character = (TextView) view.findViewById(R.id.tv_character);
            keyBackground = view.findViewById(R.id.key_background);
        }

        public void bind(int position) {

            itemView.setVisibility(View.VISIBLE);
            character.setText("");

            char c = getItem(position);
            if (c == Constants.CHAR_NOTHING) {
                itemView.setVisibility(View.GONE);
            } else if (c == Constants.CHAR_DELETE) {
                if (shouldShowDeleteButton) {
                    keyBackground.setBackgroundResource(R.drawable.img_back);
                } else {
                    itemView.setVisibility(View.GONE);
                }
            } else if (c == Constants.CHAR_HINT) {
                keyBackground.setBackgroundResource(R.drawable.img_hint);
            } else {
                keyBackground.setBackgroundResource(R.drawable.bg_key);
                character.setText(Character.toString(c).toUpperCase());
            }
        }
    }
}

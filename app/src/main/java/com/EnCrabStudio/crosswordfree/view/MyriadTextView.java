package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.manager.TypefaceManager;

/**
 * Created by khanhnguyen on 12/10/2016
 */
public class MyriadTextView extends TextView {
    public MyriadTextView(Context context) {
        super(context);
        init();
    }

    public MyriadTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyriadTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setTypeface(TypefaceManager.MYRIAD);
    }
}

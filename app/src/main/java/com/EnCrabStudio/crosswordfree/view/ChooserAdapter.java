package com.EnCrabStudio.crosswordfree.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;

import java.util.List;

/**
 * Created by Admin on 9/27/2016
 */
public class ChooserAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private List<String> items;
    private String selectedItem;

    public ChooserAdapter(List<String> items, String selectedItem) {
        this.items = items;
        this.selectedItem = selectedItem;
    }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public String getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mLayoutInflater == null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            view = mLayoutInflater.inflate(R.layout.adapter_chooser_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.bind(position);
        return view;
    }

    class ViewHolder {

        private TextView tvItemName;
        private RadioButton rdSelected;

        public ViewHolder(View view) {
            tvItemName = (TextView) view.findViewById(R.id.tv_item_name);
            rdSelected = (RadioButton) view.findViewById(R.id.rd_selected);
        }

        public void bind(int position) {
            String item = getItem(position);
            tvItemName.setText(item);
            rdSelected.setChecked(item.equals(selectedItem));
        }
    }
}

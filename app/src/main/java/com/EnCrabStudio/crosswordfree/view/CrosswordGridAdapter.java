package com.EnCrabStudio.crosswordfree.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.crossword.Grid;
import com.EnCrabStudio.crosswordfree.crossword.WordCoordinate;
import com.EnCrabStudio.crosswordfree.interfaces.ICrosswordCellListener;

/**
 * Created by Admin on 9/27/2016
 */
public class CrosswordGridAdapter extends RecyclerView.Adapter<CrosswordGridAdapter.ViewHolder> {

    private ICrosswordCellListener mListener;

    private LayoutInflater mLayoutInflater;
    private Grid mOriginalGrid, mPlayingGrid;
    private WordCoordinate mCurrentWordCoordinate;
    private String currentWord;
    private int selectedPosition = -1;

    public CrosswordGridAdapter(Grid grid) {
        this.mOriginalGrid = grid;
        this.mPlayingGrid = grid.cloneForPlaying();
    }

    public CrosswordGridAdapter(Grid originalGird, Grid playerGird) {
        this.mOriginalGrid = originalGird;
        this.mPlayingGrid = playerGird;
    }

    public Character getItemAt(int position) {
        int itemCol = position % mOriginalGrid.getMaxColumns();
        int itemRow = position / mOriginalGrid.getMaxColumns();
        return mPlayingGrid.getValue(itemRow, itemCol);
    }

    public Character getAbsoluteItem(int position) {
        int itemCol = position % mOriginalGrid.getMaxColumns();
        int itemRow = position / mOriginalGrid.getMaxColumns();
        return mOriginalGrid.getValue(itemRow, itemCol);
    }

    public void setCurrentWordCoordinate(WordCoordinate mCurrentWordCoordinate) {
        this.mCurrentWordCoordinate = mCurrentWordCoordinate;
    }

    public WordCoordinate getCurrentWordCoordinate() {
        return mCurrentWordCoordinate;
    }

    public Grid getOriginalGrid() {
        return mOriginalGrid;
    }

    public Grid getPlayingGrid() {
        return mPlayingGrid;
    }

    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public void setCellClickListener(ICrosswordCellListener listener) {
        this.mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mLayoutInflater == null)
            mLayoutInflater = LayoutInflater.from(parent.getContext());

        return new ViewHolder(mLayoutInflater.inflate(R.layout.cell_crossword, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return mOriginalGrid.getMaxColumns() * mOriginalGrid.getMaxRows();
    }

    public String getCurrentWord() {
        return currentWord;
    }

    /**
     * Put character to current selecting word
     *
     * @param c character
     * @return true if selectedPositionChanged -> change Keyboard
     */
    public boolean putCharacter(char c) {

        boolean positionChanged = false;

        int itemCol = selectedPosition % mOriginalGrid.getMaxColumns();
        int itemRow = selectedPosition / mOriginalGrid.getMaxColumns();
        mPlayingGrid.setValue(itemRow, itemCol, c);
        notifyItemChanged(selectedPosition);

        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            if (mCurrentWordCoordinate.inCoordinate(selectedPosition + 1, mOriginalGrid, currentWord)) {
                selectedPosition++;
                positionChanged = true;
                notifyItemChanged(selectedPosition);
            }
        } else {
            int newPosition = (itemRow + 1) * mOriginalGrid.getMaxRows() + itemCol;
            if (mCurrentWordCoordinate.inCoordinate(newPosition, mOriginalGrid, currentWord)) {
                selectedPosition = newPosition;
                positionChanged = true;
                notifyItemChanged(selectedPosition);
            }
        }
        return positionChanged;
    }

    public boolean delete() {
        boolean positionChanged = false;

        int itemCol = selectedPosition % mOriginalGrid.getMaxColumns();
        int itemRow = selectedPosition / mOriginalGrid.getMaxColumns();
        mPlayingGrid.setValue(itemRow, itemCol, '_');
        notifyItemChanged(selectedPosition);

        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            if (mCurrentWordCoordinate.inCoordinate(selectedPosition - 1, mOriginalGrid, currentWord)) {
                selectedPosition--;
                positionChanged = true;
                notifyItemChanged(selectedPosition);
            }
        } else {
            int newPosition = (itemRow - 1) * mOriginalGrid.getMaxRows() + itemCol;
            if (mCurrentWordCoordinate.inCoordinate(newPosition, mOriginalGrid, currentWord)) {
                selectedPosition = newPosition;
                positionChanged = true;
                notifyItemChanged(selectedPosition);
            }
        }
        return positionChanged;
    }

    public boolean isGameFinished() {
        for (int row = 0; row < mOriginalGrid.getMaxRows(); row++) {
            for (int col = 0; col < mOriginalGrid.getMaxColumns(); col++) {
                if (mPlayingGrid.getValue(row, col) == '_') {
                    return false;
                }
            }
        }
        selectedPosition = -1;
        currentWord = null;
        mCurrentWordCoordinate = null;
        return true;
    }

    public boolean isPlayerWon() {
        for (int row = 0; row < mOriginalGrid.getMaxRows(); row++) {
            for (int col = 0; col < mOriginalGrid.getMaxRows(); col++) {
                if (mPlayingGrid.getValue(row, col) != mOriginalGrid.getValue(row, col)) {
                    return false;
                }
            }
        }
        return true;
    }

    public void showCurrentWord() {
        if (mCurrentWordCoordinate.getDirection() == Grid.ORIENTATION_HORIZONTAL) {
            for (int columnIndex = 0; columnIndex < currentWord.length(); columnIndex++) {
                int changedPosition = mPlayingGrid.setValue(mCurrentWordCoordinate.getX(), mCurrentWordCoordinate.getY() + columnIndex, mOriginalGrid.getValue(mCurrentWordCoordinate.getX(), mCurrentWordCoordinate.getY() + columnIndex));
                notifyItemChanged(changedPosition);
            }
        } else {
            for (int rowIndex = 0; rowIndex < currentWord.length(); rowIndex++) {
                int changedPosition = mPlayingGrid.setValue(mCurrentWordCoordinate.getX() + rowIndex, mCurrentWordCoordinate.getY(), mOriginalGrid.getValue(mCurrentWordCoordinate.getX() + rowIndex, mCurrentWordCoordinate.getY()));
                notifyItemChanged(changedPosition);
            }
        }
    }

    public void showAnswer() {
        this.mPlayingGrid = mOriginalGrid;
        notifyDataSetChanged();
    }

    public void switchWord(WordCoordinate coordinate, String word, int newPosition) {
        if (coordinate != mCurrentWordCoordinate) {
            WordCoordinate lastCoordinate = mCurrentWordCoordinate;
            String lastWord = currentWord;

            mCurrentWordCoordinate = coordinate;
            currentWord = word;

            if (lastCoordinate != null) {
                int [] position = mOriginalGrid.getWordPostions(lastCoordinate, lastWord);
                for (int pos : position) {
                    notifyItemChanged(pos);
                }
            }

            int [] position = mOriginalGrid.getWordPostions(coordinate, word);
            for (int pos : position) {
                notifyItemChanged(pos);
            }
        }

        int lastPosition = selectedPosition;
        selectedPosition = newPosition;
        notifyItemChanged(lastPosition);
        notifyItemChanged(newPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private View background;
        private TextView character;
        private Context context;

        private int colorCellNormal, colorCellSelected;

        public ViewHolder(View view) {
            super(view);
            background = view.findViewById(R.id.cell_background);
            character = (TextView) view.findViewById(R.id.cell_character);
            context = background.getContext();

            colorCellNormal = ContextCompat.getColor(context, R.color.cell_normal);
            colorCellSelected = ContextCompat.getColor(context, R.color.cell_selected);
        }

        public void bind() {

            char c = getItemAt(getAdapterPosition());

            if (c == ' ') {
                itemView.setOnClickListener(null);
                background.setBackgroundColor(Color.BLACK);
            } else {
                itemView.setOnClickListener(this);
                background.setBackgroundColor(Color.WHITE);
                if (c != '_') {
                    if (c != 'ß') {
                        character.setText(Character.toString(c).toUpperCase());
                    } else {
                        character.setText(String.valueOf(c));
                    }
                } else {
                    character.setText("");
                }
            }

            if (getAdapterPosition() == selectedPosition) {
                background.setBackgroundColor(colorCellSelected);
            } else if (mCurrentWordCoordinate != null &&
                    mCurrentWordCoordinate.inCoordinate(getAdapterPosition(), mOriginalGrid, currentWord)) {
                background.setBackgroundColor(colorCellNormal);
            }
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onCellClicked(getAdapterPosition());
            }
        }
    }
}

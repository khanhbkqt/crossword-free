package com.EnCrabStudio.crosswordfree.task;

import android.content.Context;
import android.os.AsyncTask;

import com.EnCrabStudio.crosswordfree.manager.WordManager;

/**
 * Created by Admin on 9/28/2016
 */

public abstract class WordImporterTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    public WordImporterTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {
        WordManager.getInstance(context);
        return null;
    }

    @Override
    protected void onPreExecute() {
        onWordsImported();
    }

    public abstract void onWordsImported();
}

package com.EnCrabStudio.crosswordfree.task;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.EnCrabStudio.crosswordfree.manager.AppNotificationManager;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("AlarmReceiver", "onReceive");
        AppNotificationManager.showNotification(context);
    }
}

package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;

import com.EnCrabStudio.crosswordfree.crossword.Grid;
import com.EnCrabStudio.crosswordfree.crossword.WordCoordinate;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;
import com.EnCrabStudio.crosswordfree.view.CrosswordGridAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Admin on 9/28/2016
 */

public class GameManager {

    private static GameManager instance;

    private CrosswordGridAdapter mGameGridAdapter;
    private Hashtable<WordCoordinate, String> mWordCoordinateHashtable;

    private int gameSize;
    private int difficulty;

    public static GameManager getInstance() {
        if (instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    public void newGame(int size, int difficulty, CrosswordGridAdapter gameAdapter, Hashtable<WordCoordinate, String> wordCoordinateHashtable) {
        this.gameSize = size;
        this.difficulty = difficulty;
        this.mGameGridAdapter = gameAdapter;
        this.mWordCoordinateHashtable = wordCoordinateHashtable;
    }

    public void saveGameState(Context context) {
        SharedPreferenceUtil.saveObjectToSharedPreference(context, Constants.GENERATED_BOARD, mGameGridAdapter.getOriginalGrid());
        SharedPreferenceUtil.saveObjectToSharedPreference(context, Constants.PLAYER_BOARD, mGameGridAdapter.getPlayingGrid());
        SharedPreferenceUtil.putValue(context, Constants.GAME_DIFFICULTY, difficulty);
        if (saveWordCoordinateTable(context)) {
            SharedPreferenceUtil.putValue(context, Constants.GAME_SAVED, true);
        } else {
            SharedPreferenceUtil.putValue(context, Constants.GAME_SAVED, false);
        }
    }

    public boolean restoreGameState(Context context) {
        Grid originalGird = SharedPreferenceUtil.getSavedObjectFromPreference(context, Constants.GENERATED_BOARD, Grid.class);
        Grid playerGird = SharedPreferenceUtil.getSavedObjectFromPreference(context, Constants.PLAYER_BOARD, Grid.class);
        difficulty = SharedPreferenceUtil.getInt(context, Constants.GAME_DIFFICULTY, 0);
        if (restoreWordCoordinateTable(context)) {
            mGameGridAdapter = new CrosswordGridAdapter(originalGird, playerGird);
            return true;
        }
        return false;
    }

    private boolean saveWordCoordinateTable(Context context) {
        try {
            File file = new File(context.getDir("data", MODE_PRIVATE), "map");
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(mWordCoordinateHashtable);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean restoreWordCoordinateTable(Context context) {
        try {
            File file = new File(context.getDir("data", MODE_PRIVATE), "map");
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
            mWordCoordinateHashtable = (Hashtable<WordCoordinate, String>) inputStream.readObject();
            inputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    public CrosswordGridAdapter getGameAdapter() {
        return mGameGridAdapter;
    }

    public CrosswordGridAdapter getGameGridAdapter() {
        return mGameGridAdapter;
    }

    public Hashtable<WordCoordinate, String> getWordCoordinateHashtable() {
        return mWordCoordinateHashtable;
    }

    public int getGameSize() {
        return gameSize;
    }

    public int getDifficulty() {
        return difficulty;
    }
}

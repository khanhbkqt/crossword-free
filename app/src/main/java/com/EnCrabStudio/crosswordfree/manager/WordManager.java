package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;
import android.util.Log;

import com.EnCrabStudio.crosswordfree.helper.WordImporter;
import com.EnCrabStudio.crosswordfree.model.Word;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Admin on 9/26/2016
 */
public class WordManager {

    private static WordManager instance;
    private Map<String, String> mAllWords;

    private WordManager(Map<String, String> words) {
        this.mAllWords = words;
    }

    public synchronized static WordManager getInstance(Context context) {
        if (instance == null) {
            WordImporter importer = new WordImporter(context);
            instance = new WordManager(importer.importAll());
            Log.d("WordManager", instance.getAllWords().size() + " words imported.");
        }
        return instance;
    }


    public Map<String, String> getAllWords() {
        return mAllWords;
    }
}

package com.EnCrabStudio.crosswordfree.manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;

/**
 * Created by Admin on 10/3/2016
 */

public class RatingManager {

    private static RatingManager instance;
    private Context context;

    public RatingManager(Context context) {
        this.context = context;
    }

    public static void init(Context context) {
        instance = new RatingManager(context);
    }

    public static RatingManager getInstance() {
        return instance;
    }

    private boolean shouldReminderRating() {
        return !SharedPreferenceUtil.getBoolean(context, Constants.KEY_RATED, false);
    }

    public void showRatingReminderIfNeeded(final Activity context) {

        if (!shouldReminderRating()) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light))
                .setPositiveButton(R.string.text_rate_app, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AppUtils.openAppStoreLink(context);
                        SharedPreferenceUtil.putValue(context, Constants.KEY_RATED, true);
                    }
                })
                .setNegativeButton(R.string.text_no_thanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferenceUtil.putValue(context, Constants.KEY_RATED, true);
                    }
                })
                .setNeutralButton(R.string.text_later, null)
                .setTitle(R.string.text_like_this_game)
                .setMessage(R.string.text_please_rate_to_support_future_updates);
        builder.show();
    }
}

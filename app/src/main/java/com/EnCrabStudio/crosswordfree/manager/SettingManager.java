package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;
import android.os.Build;

import com.EnCrabStudio.crosswordfree.GameActivity;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;
import com.EnCrabStudio.crosswordfree.model.AdItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khanhnguyen on 29/09/2016
 */
public class SettingManager {

    private static SettingManager instance;
    private String rawJSONSetting;

    private String packageId;
    private String serverVersion;
    private String updateNotification;
    private boolean shouldCheckUpdate;
    private int checkUpdateDelay;

    private boolean shouldShowPushIcon;
    private int adsShowingDelay;
    private List<AdItem> ads;

    public static SettingManager getInstance() {
        return instance;
    }

    private SettingManager(String rawJSON) {
        if (rawJSON == null || rawJSON.isEmpty()) {
            instance = null;
        } else {
            this.rawJSONSetting = rawJSON == null ? "" : rawJSON;
            loadSetting();

            instance = this;
        }
    }

    public static void init(String rawJSONSetting) {
        new SettingManager(rawJSONSetting);
    }

    private void loadSetting() {

        try {
            JSONObject jsonObject = new JSONObject(rawJSONSetting);
            JSONArray settings = jsonObject.getJSONArray("EnCrabStudio");

            JSONObject jsonPackage = settings.getJSONObject(0);
            JSONObject jsonCheckUpdate = settings.getJSONObject(1);
            JSONObject jsonAds = settings.getJSONObject(2);

            packageId = jsonPackage.getString("pakageID");

            serverVersion = jsonCheckUpdate.getString("version");
            shouldCheckUpdate = "check".equals(jsonCheckUpdate.getString("action"));
            updateNotification = jsonCheckUpdate.getString("notification");
            checkUpdateDelay = Integer.parseInt(jsonCheckUpdate.getString("timestart"));

            shouldShowPushIcon = "push".equals(jsonAds.getString("action"));
            adsShowingDelay = Integer.parseInt(jsonAds.getString("time"));
            ads = parseAdList(jsonAds.getJSONObject("ads").getJSONArray("ad"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean canShowPushIcon(Context context) {
        int appLaunchCount = SharedPreferenceUtil.getInt(context, Constants.KEY_APP_LAUNCH_COUNT, 0);
        return !rawJSONSetting.isEmpty() && shouldShowPushIcon && appLaunchCount % 5 == 0 && ads != null && !ads.isEmpty();
    }

    public boolean isPackageNameValid(Context context) {
        return context.getPackageName().equals(packageId);
    }

    public List<AdItem> getAds() {
        return ads;
    }

    public int getCheckUpdateDelay() {
        return checkUpdateDelay;
    }

    public int getAdsShowingDelay() {
        return adsShowingDelay * 1000;
    }

    public String getUpdateNotification() {
        return updateNotification;
    }

    public boolean shouldCheckUpdate(Context context) {
        String currentVersion = Build.VERSION.RELEASE;
        return !rawJSONSetting.isEmpty() && shouldCheckUpdate && !currentVersion.equals(serverVersion);
    }

    public boolean canShowGoogleAds(Context context) {
        return SharedPreferenceUtil.getBoolean(context, Constants.KEY_GOOGLE_ADS_VISIBLE, true);
    }

    private List<AdItem> parseAdList(JSONArray jsonAd) {
        List<AdItem> ads = new ArrayList<>();
        try {
            for (int i = 0; i < jsonAd.length(); i++) {
                JSONObject ad = jsonAd.getJSONObject(i);
                AdItem adItem = new AdItem(ad.getString("image"), ad.getString("url"));
                ads.add(adItem);
            }
        } catch (JSONException e) {

        }
        return ads;
    }
}

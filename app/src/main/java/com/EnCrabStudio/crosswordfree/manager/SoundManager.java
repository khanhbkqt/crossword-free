package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.helper.Constants;
import com.EnCrabStudio.crosswordfree.helper.SharedPreferenceUtil;
import com.EnCrabStudio.crosswordfree.interfaces.ICrosswordSound;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public class SoundManager implements SoundPool.OnLoadCompleteListener, ICrosswordSound {

    private static SoundManager instance;
    private Context context;
    private SoundPool mSoundPool;
    private AudioManager mAudioManager;

    private int mClickSoundID, mWinSoundID, mLoseSoundID;
    private boolean mClickSoundLoaded, mWinSoundLoaded, mLoseSoundLoaded;

    private float actVolume, maxVolume, volume;
    private boolean mAppSoundEnabled = false;

    private SoundManager(Context context) {
        this.context = context;
        instance = this;

        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mSoundPool = buildSoundPool();

        mSoundPool.setOnLoadCompleteListener(this);

        actVolume = (float) mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = (float) mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        volume = actVolume / maxVolume;

        mClickSoundID = mSoundPool.load(context, R.raw.click_sound, 1);
        mWinSoundID = mSoundPool.load(context, R.raw.won_sound, 1);
        mLoseSoundID = mSoundPool.load(context, R.raw.lose_sound, 1);

        // Sounds is enabled by default
        mAppSoundEnabled = SharedPreferenceUtil.getBoolean(context, Constants.KEY_APP_SOUND_ENABLED, true);
    }

    public static void init(Context context) {
        Log.d("SoundManager", "init SoundManager");
        new SoundManager(context);
    }

    public static SoundManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("SoundManager is not initialized");
        }
        return instance;
    }

    public void setAppSoundEnabled(boolean enabled) {
        SharedPreferenceUtil.putValue(context, Constants.KEY_APP_SOUND_ENABLED, enabled);
        mAppSoundEnabled = enabled;
    }

    private SoundPool buildSoundPool() {

        SoundPool soundPool;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setMaxStreams(10)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
            return soundPool;
        }
        return soundPool;
    }

    @Override
    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
        if (status == 0) {
            if (sampleId == mClickSoundID) {
                mClickSoundLoaded = true;
            } else if (sampleId == mWinSoundID) {
                mWinSoundLoaded = true;
            }else if (sampleId == mLoseSoundID) {
                mLoseSoundLoaded = true;
            }
        }
    }

    @Override
    public void playClickSound() {
        if (mAppSoundEnabled && mClickSoundLoaded) {
            mSoundPool.play(mClickSoundID, volume, volume, 0, 0, 1);
        }
    }

    @Override
    public void playWinSound() {
        if (mAppSoundEnabled && mWinSoundLoaded) {
            mSoundPool.play(mWinSoundID, volume, volume, 0, 0, 1);
        }
    }

    @Override
    public void playLoseSound() {
        if (mAppSoundEnabled && mLoseSoundLoaded) {
            mSoundPool.play(mLoseSoundID, volume, volume, 0, 0, 1);
        }
    }

    public boolean isAppSoundEnabled() {
        return mAppSoundEnabled;
    }
}

package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by khanhnguyen on 12/10/2016
 */
public class TypefaceManager {

    public static Typeface MYRIAD;
    public static Typeface SCRIPTINA;

    public static void init(Context context) {
        MYRIAD = Typeface.createFromAsset(context.getAssets(), "Myriad.ttf");
        SCRIPTINA = Typeface.createFromAsset(context.getAssets(), "Scriptina.ttf");
    }
}

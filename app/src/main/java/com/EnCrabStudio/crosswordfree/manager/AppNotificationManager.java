package com.EnCrabStudio.crosswordfree.manager;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.EnCrabStudio.crosswordfree.MainActivity;
import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.task.AlarmReceiver;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public class AppNotificationManager {

    private static final long NOTIFICATION_INTERVAL = 2 * 24 * 60 * 60 * 1000;

    private static AlarmManager alarmMgr;
    private static PendingIntent alarmIntent;

    public static void cancelScheduledAlarm(Context context) {

        Log.d("AppNotificationManager", "cancelScheduledAlarm");

        clearReminderNotification(context);

        if (alarmMgr == null) {
            alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, AlarmReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        }
        if (alarmMgr != null) {
            try {
                alarmMgr.cancel(alarmIntent);
            } catch (Exception e) {
                Log.e("AppNotificationManager", "AlarmManager update was not canceled. " + e.toString());
                e.printStackTrace();
            }
        }
    }

    public static void scheduleNewAlarm(Context context) {
        cancelScheduledAlarm(context);

        Log.d("AppNotificationManager", "scheduleNewAlarm");

        if (alarmMgr == null) {
            alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, AlarmReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        alarmMgr.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() +
                        NOTIFICATION_INTERVAL, alarmIntent);
    }

    public static void showNotification(Context context) {
        Notification.Builder builder = new Notification.Builder(context)
                .setAutoCancel(true)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.text_let_come_back_and_play))
                .setTicker(context.getString(R.string.text_let_come_back_and_play))
                .setSmallIcon(R.mipmap.ic_launcher);

        Intent resultIntent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, builder.build());
        scheduleNewAlarm(context);
    }

    public static void clearReminderNotification(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(0);
    }
}

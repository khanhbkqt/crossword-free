package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;

import com.EnCrabStudio.crosswordfree.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
 * Created by khanhnguyen on 30/09/2016
 */
public class InterstitialAdManager {

    private static InterstitialAdManager instance;
    private Context context;
    private InterstitialAd mInterstitialAd;

    private InterstitialAdManager(Context context) {
        this.context = context;
        loadInterstitial();
    }

    public synchronized static void init(Context context) {
        if (instance == null)
            instance = new InterstitialAdManager(context);
    }

    public static InterstitialAdManager getInstance() {
        return instance;
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(context.getResources().getString(R.string.interstitial_ad_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdClosed() {
                loadInterstitial();
            }
        });
        return interstitialAd;
    }

    public void showInterstitial() {
        if ((SettingManager.getInstance() == null || SettingManager.getInstance().canShowGoogleAds(context)) && mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            loadInterstitial();
        }
    }

    private void loadInterstitial() {
        mInterstitialAd = newInterstitialAd();
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        mInterstitialAd.loadAd(adRequest);
    }
}

package com.EnCrabStudio.crosswordfree.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.EnCrabStudio.crosswordfree.helper.AppUtils;
import com.EnCrabStudio.crosswordfree.model.AdItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Admin on 10/3/2016
 */

public class PushAdsManager implements View.OnClickListener {

    private SettingManager mSettingManager;
    private Context context;
    private ImageView imvPushIcon;

    private List<AdItem> adItems;
    private int currentDisplayedAdIndex = -1;
    private AdItem mCurrentAd;

    private Timer mTimer;

    public PushAdsManager(@NonNull ImageView imvPushIcon) {
        this.imvPushIcon = imvPushIcon;
        this.context = imvPushIcon.getContext();
        this.imvPushIcon.setOnClickListener(this);
        this.mSettingManager = SettingManager.getInstance();
    }

    public void startPushIconDisplayer() {
        if (mSettingManager != null && mSettingManager.canShowPushIcon(context)) {
            imvPushIcon.setVisibility(View.VISIBLE);
            this.adItems = mSettingManager.getAds();
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    displayNextAd();
                }
            }, 0, mSettingManager.getAdsShowingDelay());
        } else {
            imvPushIcon.setVisibility(View.GONE);
        }
    }

    public void stopDisplayer() {
        if (mTimer != null)
            mTimer.cancel();
    }

    private void displayNextAd() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                currentDisplayedAdIndex++;
                if (currentDisplayedAdIndex > adItems.size() - 1) {
                    currentDisplayedAdIndex = 0;
                }
                final AdItem currentAd = adItems.get(currentDisplayedAdIndex);
                Picasso.with(context).load(currentAd.getImage()).into(imvPushIcon, new Callback() {
                    @Override
                    public void onSuccess() {
                        mCurrentAd = currentAd;
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (mCurrentAd != null) {
            AppUtils.openAppStoreLink(context, mCurrentAd.getPackageName());
        }
    }
}

package com.EnCrabStudio.crosswordfree.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;
import com.EnCrabStudio.crosswordfree.view.ChooserAdapter;

import java.util.List;

/**
 * Created by Admin on 9/28/2016
 */

public class ChooserDialog extends Dialog {

    private List<String> items;
    private String selectedItem;
    private String title;

    private ChooserAdapter mChooserAdapter;
    private ChooserCallback callback;

    private TextView mTitleTextView;
    private ListView mItemListView;

    public ChooserDialog(Context context, String title, List<String> items, String selectedItem, ChooserCallback callback) {
        super(context);
        this.items = items;
        this.selectedItem = selectedItem;
        this.title = title;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable());

        setContentView(R.layout.dialog_chooser);

        mTitleTextView = (TextView) findViewById(R.id.tv_title);
        mItemListView = (ListView) findViewById(R.id.list_view);

        mChooserAdapter = new ChooserAdapter(items, selectedItem);
        mItemListView.setAdapter(mChooserAdapter);
        mItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SoundManager.getInstance().playClickSound();
                callback.onChose(position);
                dismiss();
            }
        });

        mTitleTextView.setText(title);
    }

    public interface ChooserCallback {
        void onChose(int selectedIndex);
    }
}

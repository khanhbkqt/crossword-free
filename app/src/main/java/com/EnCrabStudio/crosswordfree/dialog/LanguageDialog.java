package com.EnCrabStudio.crosswordfree.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;
import com.EnCrabStudio.crosswordfree.model.Language;
import com.EnCrabStudio.crosswordfree.view.LanguageAdapter;

/**
 * Created by Admin on 9/28/2016
 */

public class LanguageDialog extends Dialog {

    private LanguageAdapter mLanguageAdapter;
    private ChooserCallback callback;

    private TextView mTitleTextView;
    private GridView mItemGrid;

    public LanguageDialog(Context context, ChooserCallback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable());

        setContentView(R.layout.dialog_language);

        mTitleTextView = (TextView) findViewById(R.id.tv_title);
        mItemGrid = (GridView) findViewById(R.id.grid_view);
        mItemGrid.setNumColumns(2);

        mLanguageAdapter = new LanguageAdapter(getContext());
        mItemGrid.setAdapter(mLanguageAdapter);
        mItemGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SoundManager.getInstance().playClickSound();
                Language language = mLanguageAdapter.getItem(position);
                if (!language.getPackageName().equals(getContext().getPackageName())) {
                    callback.onChose(language);
                }
                dismiss();
            }
        });
    }

    public interface ChooserCallback {
        void onChose(Language language);
    }
}

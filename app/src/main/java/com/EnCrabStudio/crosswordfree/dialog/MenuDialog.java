package com.EnCrabStudio.crosswordfree.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.manager.SoundManager;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public class MenuDialog extends Dialog implements View.OnClickListener {

    private MenuDialogCallback menuDialogCallback;
    private View btnSolve, btnBack, btnExit, btnHint;

    public MenuDialog(Context context, MenuDialogCallback callback) {
        super(context);
        this.menuDialogCallback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable());

        setContentView(R.layout.dialog_menu);

        btnSolve = findViewById(R.id.btn_solve);
        btnBack = findViewById(R.id.btn_back);
        btnExit = findViewById(R.id.btn_exit);
        btnHint = findViewById(R.id.btn_hint);

        btnSolve.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        btnHint.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        SoundManager.getInstance().playClickSound();
        dismiss();

        if (v == btnSolve) {
            menuDialogCallback.onSolveButtonClicked();
        } else if (v == btnBack) {
            menuDialogCallback.onBackButtonClicked();
        } else if (v == btnExit) {
            menuDialogCallback.onExitButtonClicked();
        } else if (v == btnHint) {
            menuDialogCallback.onHintButtonClicked();
        }
    }

    public interface MenuDialogCallback {
        void onHintButtonClicked();
        void onSolveButtonClicked();
        void onBackButtonClicked();
        void onExitButtonClicked();
    }
}

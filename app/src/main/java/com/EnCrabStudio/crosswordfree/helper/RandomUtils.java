package com.EnCrabStudio.crosswordfree.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Admin on 9/28/2016
 */

public class RandomUtils {

    private static Random random = new Random();

    public static char rndChar(String characters) {
        return characters.charAt(random.nextInt(characters.length()));
    }

    public static List<Character> randomCharArray(String allChars, int size, String word) {
        List<Character> chars = new ArrayList<>();
        for (char c : word.toCharArray()) {
            chars.add(c);
        }
        while (chars.size() < size) {
            char randomChar = rndChar(allChars);
            chars.add(randomChar);
        }
        Collections.shuffle(chars);
        return chars;
    }
}

package com.EnCrabStudio.crosswordfree.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.GridView;

import com.EnCrabStudio.crosswordfree.R;
import com.EnCrabStudio.crosswordfree.model.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by khanhnguyen on 03/10/2016
 */
public class AppUtils {

    public static void openShareIntent(Context context) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Constants.APP_LINK);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static void openDeveloperLink(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://dev?id=" + Constants.DEVELOPER_ID)));
        } catch (android.content.ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.DEVELOPER_LINK)));
        }
    }

    public static void openAppStoreLink(Context context) {
        openAppStoreLink(context, context.getPackageName());
    }

    public static void openAppStoreLink(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (android.content.ActivityNotFoundException e) {
            String storeLink = String.format(Constants.STORE_LINK, packageName);
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(storeLink)));
        }
    }

    public static List<Language> loadLanguageList(Context context) {
        List<Language> languages = new ArrayList<>();
        String[] languageNames = context.getResources().getStringArray(R.array.languages);
        String[] packageNames = context.getResources().getStringArray(R.array.app_package_names);

        TypedArray ar = context.getResources().obtainTypedArray(R.array.language_icons);
        int len = ar.length();

        int[] drawableIds = new int[len];

        for (int i = 0; i < len; i++)
            drawableIds[i] = ar.getResourceId(i, 0);

        ar.recycle();

        for (int i = 0; i < languageNames.length; i++) {
            languages.add(new Language(packageNames[i], languageNames[i], drawableIds[i]));
        }

        return languages;
    }

    public static void appLaunchCounter(Context context) {
        int appLaunchCount = SharedPreferenceUtil.getInt(context, Constants.KEY_APP_LAUNCH_COUNT, 0);
        SharedPreferenceUtil.putValue(context, Constants.KEY_APP_LAUNCH_COUNT, appLaunchCount + 1);
    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    public static void calculateActualGridWidth(final Context context, final GridView gridView, int numColumns) {

        int dp = (int) dpAsPixels(context, 1);
        int padding = (numColumns + 1) * dp;

        int gridWidth = gridView.getColumnWidth() * numColumns + padding;

        float columnWidth = (getScreenWidth(context) - padding) / numColumns;

        System.out.println("gridWidth = " + gridWidth + ". getScreenWidth = " + getScreenWidth(context));
        gridView.setColumnWidth((int) columnWidth);
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics.widthPixels;
    }

    public static float dpAsPixels(Context context, float dp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
}

package com.EnCrabStudio.crosswordfree.helper;

/**
 * Created by Admin on 9/28/2016
 */

public class Constants {

    public static final String JSON_CONFIG_FILE_URL = "https://dl.dropboxusercontent.com/s/7u8w39bzxdos8ra/Crossword2EN.json";
    public static final String APP_LINK = "https://play.google.com/store/apps/details?id=com.EnCrabStudio.crosswordfree";
    public static final String STORE_LINK = "https://play.google.com/store/apps/details?id=%s";
    public static final String DEVELOPER_LINK = "https://play.google.com/store/apps/dev?id=7979816630421656938";
    public static final String DEVELOPER_ID = "7979816630421656938";

    public static final int REQUEST_PLAY_GAME = 1001;

    public static final String KEY_NEW_GAME = "key_new_game";
    public static final String KEY_BOARD_SIZE = "key_board_size";
    public static final String KEY_DIFFICULTY = "key_difficulty";
    public static final String KEY_APP_SOUND_ENABLED = "app_sound_enabled";
    public static final String KEY_RATED = "app_rated";
    public static final String KEY_APP_LAUNCH_COUNT = "app_launch_count";
    public static final String KEY_LAST_GAME_SIZE = "last_game_size";
    public static final String KEY_LAST_GAME_DIFFICULT = "last_game_difficult";
    public static final String KEY_SETTING_INITIALIZED = "setting_initialized";
    public static final String KEY_GOOGLE_ADS_VISIBLE = "show_google_ads";

    public static final String GENERATED_BOARD = "generated_board";
    public static final String PLAYER_BOARD = "player_board";
    public static final String GAME_SAVED = "game_saved";
    public static final String GAME_DIFFICULTY = "game_difficulty";
    public static final char CHAR_DELETE = '<';
    public static final char CHAR_HINT = '_';
    public static final char CHAR_NOTHING = ' ';
}

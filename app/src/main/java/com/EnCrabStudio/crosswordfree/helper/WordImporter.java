package com.EnCrabStudio.crosswordfree.helper;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 9/27/2016
 */

public class WordImporter {

    private Context mContext;
    private AssetManager mAssetManager;

    public WordImporter(Context context) {
        this.mContext = context.getApplicationContext();
        mAssetManager = mContext.getAssets();
    }

    public Map<String, String> importAll() {
        Map<String, String> results = new HashMap<>();
        try {
            InputStream is = mAssetManager.open("data.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String line;
            while ((line = br.readLine()) != null) {
                String [] parts = line.split("\\t");
                results.put(parts[0].trim().toUpperCase(), parts[1].trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return results;
    }
}
